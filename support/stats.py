#!/usr/bin/python3
##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################


# Print benchmarks statistics

import argparse
import os
import subprocess
import json
import glob
from common import *


# Get the command line arguments
parser = argparse.ArgumentParser(description='Statistics script of Juliet benchmarks.')
parser.add_argument('path',
                    help="Path results directory")

args = parser.parse_args()

# Search all JSON files in target directory
files = glob.glob(args.path + '/summary/**/*.json', recursive = True)

# Collect stats
stats = {}
for f in files:
    with open(f, 'r') as fp:
        data = json.load(fp)
        cwe = data['cwe-code']
        status = data['status']
        time = data['time']
        if cwe not in stats:
            stats[cwe] = {
                'total': 0,
                'time': 0.0,
                'Success': 0,
                'Imprecise': 0,
                'Unsound': 0,
                'Failure': 0,
                'Unsupported': 0
            }
        stats[cwe]['total'] += 1
        stats[cwe]['time'] += time
        stats[cwe][status] += 1


# Utility function to format time
def format_time(seconds):
    hours, rem = divmod(seconds, 3600)
    minutes, seconds = divmod(rem, 60)
    return "{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds)

# Utility function to format percentages
def format_percentage(n,total):
    return "%d%%"%(int(100*n/total))

# Print stats
print("+-{:-<7}-+-{:-<30}-+-{:-<15}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+".format('','','','','','','','',''))
print("| {:^7} | {:^30} | {:^15} | {:^11} | {:^11} | {:^11} | {:^11} | {:^11} | {:^11} |".format(
    'CWE',
    'Title',
    'Time',
    'Total',
    'Success',
    'Imprecise',
    'Unsound',
    'Failure',
    'Unsupported'))
print("+-{:-<7}-+-{:-<30}-+-{:-<15}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+".format('','','','','','','','',''))

total = {
    'total': 0,
    'time': 0.0,
    'Success': 0,
    'Imprecise': 0,
    'Unsound': 0,
    'Failure': 0,
    'Unsupported': 0
}

for cwe in sorted(stats.keys()):
    name = cwe_name[cwe] if len(cwe_name[cwe]) < 30 else cwe_name[cwe][:27] + '..'
    print("| {:^7} | {:^30} | {:^15} | {:^11} | {:^11} | {:^11} | {:^11} | {:^11} | {:^11} |".format(
        'CWE%d'%cwe,
        name,
        format_time(stats[cwe]['time']),
        stats[cwe]['total'],
        format_percentage(stats[cwe]["Success"], stats[cwe]['total']),
        format_percentage(stats[cwe]["Imprecise"], stats[cwe]['total']),
        format_percentage(stats[cwe]["Unsound"], stats[cwe]['total']),
        format_percentage(stats[cwe]["Failure"], stats[cwe]['total']),
        format_percentage(stats[cwe]["Unsupported"], stats[cwe]['total'])))
    total['time'] += stats[cwe]['time']
    total['total'] += stats[cwe]['total']
    total['Success'] += stats[cwe]['Success']
    total['Imprecise'] += stats[cwe]['Imprecise']
    total['Unsound'] += stats[cwe]['Unsound']
    total['Failure'] += stats[cwe]['Failure']
    total['Unsupported'] += stats[cwe]['Unsupported']


print("+-{:-<7}-+-{:-<30}-+-{:-<15}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+".format('','','','','','','','',''))
print("| {:^40} | {:^15} | {:^11} | {:^11} | {:^11} | {:^11} | {:^11} | {:^11} |".format(
    'Total',
    format_time(total['time']),
    total['total'],
    format_percentage(total["Success"], total['total']),
    format_percentage(total["Imprecise"], total['total']),
    format_percentage(total["Unsound"], total['total']),
    format_percentage(total["Failure"], total['total']),
    format_percentage(total["Unsupported"], total['total'])))
print("+-{:-<40}-+-{:-<15}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+-{:-<11}-+".format('','','','','','','','','',''))

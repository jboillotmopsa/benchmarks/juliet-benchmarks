##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################


# Generic Makefile for running Mopsa on Juliet testcases


##############
# Parameters #
##############

# Path to Juliet Test Suite
JULIET ?= src

# Mopsa configuration to use (without the leading c/ prefix)
CONFIG ?= cell-string-length-itv-zero

# Path to mopsa-c binary
MOPSA  ?= mopsa-c

# Directory containing the results
OUTPUT ?= results


#############
# Constants #
#############

# Zip file containing Juliet sources
ZIP := 2017-10-01-juliet-test-suite-for-c-cplusplus-v1-3.zip

# Link to Juliet zip file
URL := https://samate.nist.gov/SARD/downloads/test-suites/${ZIP}

# List of tests to analyze
TESTS := $(patsubst ${JULIET}/C/testcases/%,%,\
	 $(strip \
	 $(shell \
		find ${JULIET}/C/testcases -regextype posix-egrep -regex ".*[0-9]a?\.c" | \
		sed "s/a\.c/\.c/" | \
		sed "s/\.c//")))

# List of CWEs
CWE := $(patsubst ${JULIET}/C/testcases/%,%,$(wildcard ${JULIET}/C/testcases/*))

# List of supported CWEs
INCLUDED := $(shell cat support/cwe.txt)

# List of unsupported tests
EXCLUDED := $(shell cat support/exclude.txt)

# Flags of the script support/analyze.py
FLAGS = --config=c/${CONFIG}.json --mopsa=${MOPSA}


ifneq ($(VERBOSE),)
FLAGS += --verbose
endif


##################
# Analysis rules #
##################

.PHONY: all ${CWE} ${TESTS} stats clean prepare ci

all: ${CWE}

prepare: ${JULIET}

${JULIET}:
	wget ${URL}
	unzip ${ZIP} -d ${JULIET}
	rm ${ZIP}
	patch -p0 < support/mopsa.patch
	find ${JULIET}/C/testcases -iname "*wchar*" | xargs rm -rf
	find ${JULIET}/C/testcases -iname "*.cpp" | xargs rm -rf
	find ${JULIET}/C/testcases -mindepth 1 -maxdepth 1 -type d | grep -xv $(patsubst %,-e ${JULIET}/C/testcases/% ,${INCLUDED}) | xargs rm -rf
	rm -rf $(patsubst %,${JULIET}/C/testcases/%*.c,${EXCLUDED})

.SECONDEXPANSION:
${CWE}: $$(sort $$(filter $$@%,${TESTS}))


${TESTS}:
	@mkdir -p $(dir ${OUTPUT}/summary/$@)
	@mkdir -p $(dir ${OUTPUT}/details/$@)
	@support/analyze.py ${JULIET}/C/testcases/$@ --summary-output-dir=$(dir ${OUTPUT}/summary/$@) --details-output-dir=$(dir ${OUTPUT}/details/$@) ${FLAGS}

stats:
	@support/stats.py ${OUTPUT}

ci:
	support/genci.py --juliet=${JULIET} > .gitlab-ci.yml

clean:
	@rm -rf ${OUTPUT}/summary ${OUTPUT}/details

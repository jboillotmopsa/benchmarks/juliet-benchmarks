# Docker image to run Mospa on Juliet Test Suite
# Build with: docker build -t mopsa-juliet .
#
###############################################

FROM mopsa/mopsa-build:latest


# copy necessary files
##

COPY Makefile /tmp/
COPY support/cwe.txt /tmp/support/
COPY support/exclude.txt /tmp/support/
COPY support/mopsa.patch /tmp/support/
WORKDIR /tmp


# install Juliet
##

RUN \
    make prepare && \
    mv src /home/mopsa/juliet
